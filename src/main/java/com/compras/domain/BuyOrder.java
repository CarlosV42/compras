package com.compras.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * Created by CarlosV on 24/8/2018.
 */
@Entity
public class BuyOrder extends ModelBase {
    private String code;
    @ManyToOne(optional = false)
    private Buyer buyer;
    @OneToOne(optional = false)
    private Product product;
    @OneToOne(optional = false)
    private Incident incident;

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }
}
