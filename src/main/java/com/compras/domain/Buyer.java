package com.compras.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CarlosV on 24/8/2018.
 */
@Entity
public class Buyer extends ModelBase {
    private String name;
    private String nitCi;
    @OneToMany(mappedBy = "buyer", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<BuyOrder> buyOrders = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNitCi() {
        return nitCi;
    }

    public void setNitCi(String nitCi) {
        this.nitCi = nitCi;
    }
}
