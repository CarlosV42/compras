package com.compras.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by CarlosV on 24/8/2018.
 */
@Entity
public class IncidentRegistry extends ModelBase {
    @Column
    private String code;

    @OneToMany(mappedBy = "incidentRegistry", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<Incident> incidents = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
