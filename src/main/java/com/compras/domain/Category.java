package com.compras.domain;

import javax.persistence.Entity;

/**
 * Created by CarlosV on 24/8/2018.
 */
@Entity
public class Category extends ModelBase {
    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
