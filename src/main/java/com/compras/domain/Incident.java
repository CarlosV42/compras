package com.compras.domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * Created by CarlosV on 24/8/2018.
 */
@Entity
public class Incident extends ModelBase {
    private String code;
    @ManyToOne(optional = false)
    private IncidentRegistry incidentRegistry;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public IncidentRegistry getIncidentRegistry() {
        return incidentRegistry;
    }

    public void setIncidentRegistry(IncidentRegistry incidentRegistry) {
        this.incidentRegistry = incidentRegistry;
    }
}
